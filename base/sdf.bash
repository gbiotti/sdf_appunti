#!/usr/bin/env bash
echo "Per eliminare l'utente usare   : 'userdel -r <username>'"
echo "Per creare l'utente usare      : 'useradd -m <username>'"
echo "Per impostare la password usare: 'passwd'"
echo ""
echo "Se quando si elimina l'utente non viene eliminata anche la \$HOME"
echo "puo' dipendere dal fatto che esistono processi in esecuzione'"
echo "lanciati dall'utente, userdel lo dice."
echo ""
echo ""
if [ $(id -u) -eq 0 ]; then
    USN="sdf"
    USP="sdf"
	egrep "^$USN" /etc/passwd >/dev/null
	if [ $? -eq 0 ]; then
        echo "Sto per eliminare l'utente $USN"
		userdel -r $USN
        echo "Ho eliminato l'utente $USN"
    else
        echo "L'utente $USN non esiste ancora"
	fi
    echo "Proseguo con la creazione dell'utente $USN"
    useradd -m $USN
    echo $USN:$USP | chpasswd
	echo "Utente $USN aggiunto al sistema"
else
	echo "Script eseguile soltanto come root!!!"
	exit 2
fi
