* Appunti per la "Serata Dotfiles"

  La serata è dedicata alla gestione dei /dotfiles/.

  Nel repository sono presenti files e directory utili per la serata.
  - [[seratadotfiles.org][File con gli appunti sull'argomento]] ([[seratadotfiles.pdf][anche in versione pdf]])
  - [[homerepo.org][File con gli appunti sul ~bare repository~ nella home]]
  - [[gnu_stow.org][File con gli appunti su ~GNU Stow~]]
  - [[dotbot.org][File con gli appunti su ~dotbot~]]
  - [[chezmoi.org][File con gli appunti su ~chezmoi~]]
  - [[yadm.org][File con gli appunti su ~yadm~]]
  - [[base][Directory con elementi di base per le demo]]

  Ci sono poi altri repository utilizzati per le varie demo
  
